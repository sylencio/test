using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

using Amazon.Lambda.Core;
using Amazon.Lambda.Serialization;
using Amazon.EC2.Model;
using Amazon.EC2;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializerAttribute(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace StopInstances
{
    public class Function
    {
        
        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <param name="input"></param>
        /// <param name="context"></param>
        /// <returns></returns>
        public void StopInstances(List<string> InstancesId)
        {
            AmazonEC2Client ec2 = new AmazonEC2Client();
            StopInstancesRequest req = new StopInstancesRequest(InstancesId);
            ec2.StopInstancesAsync(req);
        }
    }
}
