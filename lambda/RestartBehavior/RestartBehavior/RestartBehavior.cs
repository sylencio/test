using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Text.RegularExpressions;


using Amazon.Lambda.Core;
using Amazon.EC2;
using Amazon.EC2.Model;
using Amazon.CloudWatchEvents;
using Amazon.CloudWatchEvents.Model;

// Assembly attribute to enable the Lambda function's JSON input to be converted into a .NET class.
[assembly: LambdaSerializerAttribute(typeof(Amazon.Lambda.Serialization.Json.JsonSerializer))]

namespace RestartBehavior
{
    public class Function
    {

        /// <summary>
        /// A simple function that takes a string and does a ToUpper
        /// </summary>
        /// <returns>Nothing :)</returns>
        public async Task Main()
        {
            List<List<Tag>> Tags = GetInstancesTags();
            await DeleteExistingRules();
            await CreateRules(Tags);

        }
        //Get all the tags of all the instances of the region with name matching pattern(all by default)
        public List<List<Tag>> GetInstancesTags(string InstancePrefix = ".*")
        {
            AmazonEC2Client ec2 = new AmazonEC2Client();
            System.Threading.CancellationToken token = new System.Threading.CancellationToken();
            List<Amazon.EC2.Model.Reservation> result = ec2.DescribeInstancesAsync(token).Result.Reservations;
            List<List<Tag>> output = new List<List<Tag>>();

            foreach (Amazon.EC2.Model.Reservation reservation in result)
            {
                foreach (Amazon.EC2.Model.Instance runningInstance in reservation.Instances)
                {
                    if (runningInstance.Tags.FindAll(instance => (instance.Key.Equals("Name") && Regex.IsMatch(instance.Value, InstancePrefix))).Count != 0)
                    {
                        List<Tag> tags = new List<Tag>();
                        tags.AddRange(from tag in runningInstance.Tags where tag.Key.Equals("Start") || tag.Key.Equals("Stop") select tag);
                        tags.Add(new Tag("InstanceId", runningInstance.InstanceId));
                        output.Add(tags);
                    }
                }
            }
            return output;
        }
        //public void CreateLambdaFunctions()
        //{
        //    AmazonLambdaClient Lambda = new AmazonLambdaClient();
        //    CreateFunctionRequest req = new CreateFunctionRequest();
        //    req.Code = "C:\\Users\\Administrator\\Documents\\Visual Studio 2015\\Projects\\StartInstance\\StartInstance\\bin\\Release\\netcoreapp1.0\\StartInstance.zip";
        //    Lambda.CreateFunctionAsync()


        //}

        //Foreach Stop and Start Tag, creates a CloudWatchEvent Rule scheduled on the Tag cron value that calls a Start/Stop Lambda function as target
        //The target input is the list of instances that match the cron value
        public async Task DeleteExistingRules()
        {
            AmazonCloudWatchEventsClient cwe = new AmazonCloudWatchEventsClient();
            foreach (Rule rule in cwe.ListRulesAsync().Result.Rules)
            {
                // /!\ Delete all Start* and Stop* rules
                if (Regex.IsMatch(rule.Name, "((Start)|(Stop)).*"))
                {
                    ListTargetsByRuleRequest ltreq = new ListTargetsByRuleRequest();
                    ltreq.Rule = rule.Name;
                    RemoveTargetsRequest treq = new RemoveTargetsRequest();
                    treq.Rule = rule.Name;
                    treq.Ids.AddRange(from target in cwe.ListTargetsByRuleAsync(ltreq).Result.Targets select target.Id);
                    await cwe.RemoveTargetsAsync(treq);
                    DeleteRuleRequest req = new DeleteRuleRequest();
                    req.Name = rule.Name;
                    await cwe.DeleteRuleAsync(req);
                }
            }
        }
        public async Task CreateRules(List<List<Tag>> TagsList)
        {
            AmazonCloudWatchEventsClient cwe = new AmazonCloudWatchEventsClient();
            foreach (List<Tag> Tags in TagsList)
            {
                string Start = (from t in Tags where t.Key.Equals("Start") select t.Value).FirstOrDefault();
                string Stop = (from t in Tags where t.Key.Equals("Stop") select t.Value).FirstOrDefault();
                string InstanceId = (from t in Tags where t.Key.Equals("InstanceId") select t.Value).FirstOrDefault();

                if (!(Start ==null))
                {
                    ListRulesRequest stlreq = new ListRulesRequest();
                    stlreq.NamePrefix = "Start" + Start.Replace(" ", "-").Replace("?", ".").Replace("*", "_");
                    if ((await cwe.ListRulesAsync(stlreq)).Rules.Count == 0)
                    {
                        // Create Rule with scheduled cron expression
                        PutRuleRequest streq = new PutRuleRequest();
                        streq.Name = "Start" + Start.Replace(" ","-").Replace("?",".").Replace("*","_");
                        streq.ScheduleExpression = "cron(" + Start + ")";;
                        streq.RoleArn = "arn:aws:iam::235960612000:role/lambda_exec_List_Instances";
                        streq.Description = "Start the EC2 Instances with the cron expression:" + Start;
                        PutRuleResponse stres = await cwe.PutRuleAsync(streq);
                        //Add Target and create input
                        Target stt = new Target();
                        stt.Arn = "arn:aws:lambda:eu-west-2:235960612000:function:StartInstances";
                        stt.Id = "StartInstances" + Start.Replace(" ","-").Replace("?",".").Replace("*","_");
                        stt.Input = "[\"" + InstanceId + "\"]";
                        PutTargetsRequest sttreq = new PutTargetsRequest();
                        sttreq.Rule = streq.Name;
                        sttreq.Targets = new List<Target>() { stt };
                        PutTargetsResponse sttres = await cwe.PutTargetsAsync(sttreq);
                    } else {
                        //Update the input of existing target
                        PutTargetsRequest sttreq = new PutTargetsRequest();
                        ListTargetsByRuleRequest stltreq = new ListTargetsByRuleRequest();
                        stltreq.Rule = "Start" + Start.Replace(" ","-").Replace("?",".").Replace("*","_");
                        ListTargetsByRuleResponse stltres = await cwe.ListTargetsByRuleAsync(stltreq);
                        stltres.Targets.Find(target => target.Id=="StartInstances"+Start.Replace(" ","-").Replace("?",".").Replace("*","_") ).Input = stltres.Targets.Find(target => target.Id == "StartInstances" + Start.Replace(" ", "-").Replace("?", ".").Replace("*", "_")).Input.Split(']')[0] + ",\"" + InstanceId + "\"]";
                        sttreq.Rule = "Start" + Start.Replace(" ", "-").Replace("?", ".").Replace("*", "_");
                        sttreq.Targets = stltres.Targets;
                        PutTargetsResponse sttres = await cwe.PutTargetsAsync(sttreq); 
                    }   
                }
                if (!(Stop == null))
                {
                    ListRulesRequest splreq = new ListRulesRequest();
                    splreq.NamePrefix = "Stop" + Stop.Replace(" ", "-").Replace("?", ".").Replace("*", "_");
                    if ((await cwe.ListRulesAsync(splreq)).Rules.Count == 0)
                    {
                        // Create Rule with scheduled cron expression
                        PutRuleRequest spreq = new PutRuleRequest();
                        spreq.Name = "Stop" + Stop.Replace(" ", "-").Replace("?", ".").Replace("*", "_");
                        spreq.ScheduleExpression = "cron(" + Stop + ")"; ;
                        spreq.RoleArn = "arn:aws:iam::235960612000:role/lambda_exec_List_Instances";
                        spreq.Description = "Stop the EC2 Instances with the cron expression:" + Stop;
                        PutRuleResponse spres = await cwe.PutRuleAsync(spreq);
                        //Add Target and create input
                        Target spt = new Target();
                        spt.Arn = "arn:aws:lambda:eu-west-2:235960612000:function:StopInstances";
                        spt.Id = "StopInstances" + Stop.Replace(" ", "-").Replace("?", ".").Replace("*", "_");
                        spt.Input = "[\"" + InstanceId + "\"]";
                        PutTargetsRequest sptreq = new PutTargetsRequest();
                        sptreq.Rule = spreq.Name;
                        sptreq.Targets = new List<Target>() { spt };
                        PutTargetsResponse sptres = await cwe.PutTargetsAsync(sptreq);
                    }
                    else
                    {
                        //Update the input of existing target
                        PutTargetsRequest sptreq = new PutTargetsRequest();
                        ListTargetsByRuleRequest spltreq = new ListTargetsByRuleRequest();
                        spltreq.Rule = "Stop" + Stop.Replace(" ", "-").Replace("?", ".").Replace("*", "_");
                        ListTargetsByRuleResponse spltres = await cwe.ListTargetsByRuleAsync(spltreq);
                        spltres.Targets.Find(target => target.Id == "StopInstances" + Stop.Replace(" ", "-").Replace("?", ".").Replace("*", "_")).Input = spltres.Targets.Find(target => target.Id == "StopInstances" + Stop.Replace(" ", "-").Replace("?", ".").Replace("*", "_")).Input.Split(']')[0] + ",\"" + InstanceId + "\"]";
                        sptreq.Rule = "Stop" + Stop.Replace(" ", "-").Replace("?", ".").Replace("*", "_");
                        sptreq.Targets = spltres.Targets;
                        PutTargetsResponse sptres = await cwe.PutTargetsAsync(sptreq);
                    }
                }
                continue;  
            }
        }
    }
}
